package Controller

class MenuController {
    fun selectOption(): Int {
        var answer: Int
        do {
            var goodAnswer = false
            answer = readLine()!!.toInt()

            when (answer) {
                1 -> goodAnswer = true
            }

        } while (!goodAnswer)
        return answer
    }

}