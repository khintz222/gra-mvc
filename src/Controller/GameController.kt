package Controller

import Model.Enemy
import Model.GameOperator
import Model.Player
import View.GameView
import View.MenuView
import kotlin.random.Random


class GameController {
    var gameModel = GameOperator(0, true, 0, 500, Player(100, 10, 15), Enemy(10))


    fun run() {
        while (gameModel.game)
            when (gameModel.gameState) {
                0 -> {
                    MenuView().showMenu()
                    gameModel.gameState = MenuController().selectOption()
                }
                1 -> {
                    val gameData =
                        "${gameModel.day};${gameModel.gold};${gameModel.player.def};${gameModel.player.tent};${gameModel.player.people};${gameModel.enemy.attack};${gameModel.alert}"
                    GameView(gameData).showGame()
                    gameModel.alert=""
                    when (selectOptipon()) {
                        1 -> nextDay()
                        2 -> buyTent()
                        3 -> sellTent()
                        4 -> buyDef()


                    }
                }
                else -> {
                    println("Braku obslugi tej opcji wroc pozniej.")
                    gameModel.game = false
                }

            }

        println("Przegrales")

    }

    fun nextDay() {
        gameModel.day++
        gameModel.gold += gameModel.player.people * 10

        gameModel.enemy.attack += Random.nextInt(0, (gameModel.enemy.attack * 0.3).toInt())

        generatePeoples()


        if ((Random.nextInt(0, 10)) > 5) fight()
        if (gameModel.player.def <= 0) gameModel.game = false

    }

    fun generatePeoples() {
        val maxNewPeople = ((gameModel.player.tent * 3 - gameModel.player.people) * 0.3).toInt()
        if (maxNewPeople > 0) {
            gameModel.player.people += Random.nextInt(0, maxNewPeople)
            gameModel.alert += "\nPrzybyli nowi ludzie"
        }
    }

    fun buyDef() {
        if (gameModel.gold > 100) {
            gameModel.gold -= 100
            gameModel.player.def += 10
            gameModel.alert += "Kupiono defa"
            return
        }
        gameModel.alert += "za moalo golda"


    }

    fun fight() {
        if (gameModel.player.def > gameModel.enemy.attack) {
            gameModel.player.def -= (gameModel.enemy.attack * 0.1).toInt()
            gameModel.gold -= (gameModel.gold * 0.1).toInt()
        } else if (gameModel.player.def == gameModel.enemy.attack) {
            gameModel.player.def -= (gameModel.enemy.attack * 0.3).toInt()
            gameModel.gold -= (gameModel.gold * 0.2).toInt()
        } else {
            gameModel.player.def -= (gameModel.enemy.attack * 0.5).toInt()
            gameModel.player.people -= (gameModel.player.people * 0.1).toInt()
            gameModel.gold -= (gameModel.gold * 0.3).toInt()
        }
        gameModel.alert += "\nzostales zaatakowany"
    }

    fun buyTent() {
        if (gameModel.gold > 1000) {
            gameModel.gold -= 1000
            gameModel.player.tent++
            gameModel.alert += "Kupiono Namiot"
            return
        }
        gameModel.alert += "nie kupiono namiotu za malo golda"

    }

    fun sellTent() {
        if (gameModel.player.tent > 0 && gameModel.player.tent > gameModel.player.people / 3) {
            gameModel.gold += 750
            gameModel.player.tent--
            gameModel.alert += "Sprzedano namiot"
            return
        }
        gameModel.alert += "Nie mozna sprzedac namiotu"
    }


    fun selectOptipon(): Int {

        //TODO("zrób walidacje ")
        var answer: Int
        do {
            var goodAnswer = false
            answer = readLine()!!.toInt()

            when (answer) {
                1 -> goodAnswer = true
                2 -> goodAnswer = true
                3 -> goodAnswer = true
                4 -> goodAnswer = true
            }

        } while (!goodAnswer)
        return answer
    }
}

