package Model

data class GameOperator(
    var gameState: Int,
    var game: Boolean,
    var day: Int,
    var gold: Int,
    var player: Player,
    var enemy: Enemy,
    var alert: String =" "
)